package com.tc.itfarm.web.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2016/8/13.
 */
@Controller
@RequestMapping("error")
public class ErrorAction {

    @RequestMapping("403")
    public String noPrivilege() {
        return "error/403";
    }

    @RequestMapping("400")
    public String error400() {
        return "error/400";
    }

    @RequestMapping("401")
    public String error401() {
        return "error/401";
    }

    @RequestMapping("404")
    public String error404() {
        return "error/404";
    }

    @RequestMapping("500")
    public String error500() {
        return "error/500";
    }
}
