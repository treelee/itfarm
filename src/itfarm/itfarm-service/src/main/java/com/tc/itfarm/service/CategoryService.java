package com.tc.itfarm.service;

import java.util.List;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Category;

public interface CategoryService {
	Category select(Integer id);
	
	List<Category> selectAll();
	
	PageList<Category> selectByPage(Page page);
	
	void save(Category category);
	
	void delete(Integer recordId);
}
